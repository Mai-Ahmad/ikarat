<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Customer\CustomerRequest;
use App\Http\Requests\Api\Customer\UpdateCustomerRequest;
use App\Http\Resources\Customer\CustomerResource;
use App\Http\Traits\ApiResponseTrait;
use App\Models\Customer;
use Tymon\JWTAuth\Claims\Custom;

class CustomerController extends Controller
{
    use ApiResponseTrait;


    public function store(CustomerRequest $request)
    {
        $customer = Customer::create([
            'name'              => $request->name,
            'email'             => $request->email,
            'phone'             => $request->phone,
            'notes'             => $request->notes,
            'birth_date'        => $request->birth_date,

        ]);

        return $this->apiResponse(new CustomerResource($customer), "The customer was created successfully", 201);
    }

    public function update(UpdateCustomerRequest $request, $id)
    {
        $customer = Customer::findOrFail($id);
        $customer->update([
            'name'              => $request->name,
            'email'             => $request->email,
            'phone'             => $request->phone,
            'notes'             => $request->notes,
            'birth_date'        => $request->birth_date,
        ]);
        $customer->save();

        return $this->apiResponse(new CustomerResource($customer), 'The customer updated successfully', 200);
    }

    public function index()
    {
        $customers = Customer::paginate(config('constants.PAGINATE_NUMBER'));

        return $this->apiResponse(CustomerResource::collection($customers)->resource, '20 customer per page is returned!', 200);
    }

    public function show($id)
    {
        $customer = Customer::findOrFail($id);

        return $this->apiResponse(new CustomerResource($customer), "The customer of id = $id", 200);
    }


    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);
        $customer->delete();

        return $this->apiResponse(null, "The customer has been deleted successfully!", 200);
    }
}
