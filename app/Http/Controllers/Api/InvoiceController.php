<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Invoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Resources\Invoice\InvoiceResource;
use App\Http\Requests\Api\Invoice\InvoiceRequest;
use App\Http\Traits\SaveImageTrait;

class InvoiceController extends Controller
{
    use ApiResponseTrait, SaveImageTrait;

    public function store(InvoiceRequest $request)
    {
        $user = User::findOrFail($request->user_id);
        if ($request->type == 0 || $request->type == 2) {
            $user->wallet_balance = $user->wallet_balance - $request->price;
        } else {
            $user->wallet_balance = $user->wallet_balance + $request->price;
        }
        $user->save();
        $invoice = Invoice::create([
            'user_id'                  =>  $request->user_id,
            'real_estate_id'           =>  $request->real_estate_id,
            'reservation_id'           =>  $request->reservation_id,
            'type'                     =>  $request->type,
            'price'                    =>  $request->price,
            'details'                  =>  $request->details,
            'image'                    =>  $request->file('image') ? $this->saveImage($request->file('image'), 'Images/Invoice', 400, 400) : null,
        ]);

        return $this->apiResponse(new InvoiceResource($invoice), 'The invoice has been created successfully', 201);
    }


    public function update(InvoiceRequest $request, $id)
    {
        $invoice = Invoice::findOrFail($id);

        // return $invoice->user->wallet_balance;
        if ($request->user_id != $invoice->user_id || $request->price != $invoice->price) {
            $invoice->user->wallet_balance = $invoice->user->wallet_balance - $invoice->price;
            $invoice->user->save();
            $user = User::findOrFail($request->user_id);
            $user->wallet_balance = $user->wallet_balance + $request->price;
            $user->save();
        }

        $invoice->update([
            'user_id'                  =>  $request->user_id,
            'real_estate_id'           =>  $request->real_estate_id,
            'reservation_id'           =>  $request->reservation_id,
            'type'                     =>  $request->type,
            'price'                    =>  $request->price,
            'details'                  =>  $request->details,
            'image'                    =>  $request->image,
        ]);

        return $this->apiResponse(new InvoiceResource($invoice), "The invoice has been updated successfully!", 201);
    }

    public function index($id)
    {
        $invoices = Invoice::where('user_id', '=', $id)->get();

        return $this->apiResponse(InvoiceResource::collection($invoices), "The invoices of user id = $id", 201);
    }

    public function destroy($id)
    {
        $invoice = Invoice::findOrFail($id);

        $invoice->user->wallet_balance = $invoice->user->wallet_balance - $invoice->price;
        $invoice->user->save();
        $invoice->delete();

        return $this->apiResponse(new InvoiceResource($invoice), "The wallet details has been deleted successfully!", 201);
    }
}
