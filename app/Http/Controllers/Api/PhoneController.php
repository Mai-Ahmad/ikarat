<?php

namespace App\Http\Controllers\Api;

use App\Models\Phone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Phone\UpdatePhoneRequest;
use App\Http\Resources\Phone\PhoneResource;
use App\Http\Traits\ApiResponseTrait;

class PhoneController extends Controller
{
    use ApiResponseTrait;
    /**
     * update phone.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdatePhoneRequest $request, $id)
    {
        $phone = Phone::findOrFail($id);
        $phone->update([
            'number'      => $request->number,
        ]);
        $phone->save();

        return $this->apiResponse(new PhoneResource($phone), 'The phone number was updated successfully!', 200);
    }
}
