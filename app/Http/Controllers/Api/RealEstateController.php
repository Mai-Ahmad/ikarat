<?php

namespace App\Http\Controllers\Api;

use App\Models\RealEstate;
use Illuminate\Http\Request;
use App\Models\RealEstateImages;
use App\Http\Traits\SaveImageTrait;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Resources\RealEstate\RealEstateResource;
use App\Http\Requests\Api\RealEstate\RealEstateRequest;
use App\Http\Requests\Api\RealEstate\UpdateRealEstateRequest;
use App\Http\Resources\RealEstate\RealEstateWithoutUserResource;

class RealEstateController extends Controller
{
    use ApiResponseTrait, SaveImageTrait;


    public function store(RealEstateRequest $request)
    {
        $realEstate = RealEstate::create([
            'name'                      => $request->name,
            'description'               => $request->description,
            'user_id'                   => $request->user_id,
            'address'                   => $request->address,
            'latitude'                  => $request->latitude,
            'longitude'                 => $request->longitude,
            'phone'                     => $request->phone,
            'instagram'                 => $request->instagram,
            'profit'                    => $request->profit,
            'main_image'                => $request->file('main_image') ? $this->saveImage($request->file('main_image'), 'Images/RealEstate', 400, 400) : null,
            'color'                     => $request->color,
            'show_reports'              => $request->show_reports,
            'show_invoices'             => $request->show_invoices,
        ]);

        $images = $request->file('images');
        if ($images) {
            foreach ($images as $image) {
                $image_name_DataBase = $this->saveImages($image, 'Images/RealEstate', 400, 400);
                RealEstateImages::create(['real_estate_id' => $realEstate->id, 'image' => $image_name_DataBase]);
            }
        }

        return $this->apiResponse(new RealEstateResource($realEstate), 'The real estate has been created successfully!', 201);
    }

    public function update(UpdateRealEstateRequest $request, $id)
    {
        $realEstate = RealEstate::findOrFail($id);
        $realEstate->update([
            'name'                      => $request->name,
            'description'               => $request->description,
            'user_id'                   => $request->user_id,
            'address'                   => $request->address,
            'latitude'                  => $request->latitude,
            'longitude'                 => $request->longitude,
            'phone'                     => $request->phone,
            'instagram'                 => $request->instagram,
            'profit'                    => $request->profit,
            'main_image'                => $request->file('main_image') ? $this->updateImage($realEstate, $request, 'main_image', 'Images/RealEstate', 400, 400) : $realEstate->main_image,
            'color'                     => $request->color,
            'show_reports'              => $request->show_reports,
            'show_invoices'             => $request->show_invoices,
        ]);

        return $this->apiResponse(new RealEstateResource($realEstate), 'The real estate has been updated successfully!', 200);
    }

    public function index()
    {
        if (auth()->user()->is_admin == 1) {
            $realEstates = RealEstate::paginate(config('constants.PAGINATE_NUMBER'));
        } else {
            $realEstates = RealEstate::where('user_id', auth()->user()->id)->paginate(config('constants.PAGINATE_NUMBER'));
        }

        return $this->apiResponse(RealEstateResource::collection($realEstates)->resource, '20 real estate per page is returned!', 200);
    }

    public function show($id)
    {
        $realEstate = RealEstate::findOrFail($id);

        return $this->apiResponse(new RealEstateResource($realEstate), "The real estate details of id = $id", 200);
    }


    public function getNextAvailableDate($realEstate)
    {
        // Get all reservations for the real estate, ordered by start date
        $reservations = $realEstate->reservations()->where('end_date', '>=', now()->toDateString())->orderBy('start_date')->get();

        // If there are no reservations from today and after, the real estate is available from today
        if ($reservations->isEmpty()) {
            return now()->toDateString();
        }

        $lastEndDate = null;
        $nextAvailableDate = null;

        foreach ($reservations as $reservation) {
            if (is_null($lastEndDate)) {
                // If this is the first reservation, check if there is a gap from today to the reservation start date
                if (now()->lessThanOrEqualTo($reservation->start_date)) {
                    $nextAvailableDate = now();
                    break;
                }
                $lastEndDate = $reservation->end_date;
            } else {
                // If there are multiple reservations, check if there is a gap between reservations
                if ($reservation->start_date->greaterThan($lastEndDate->addDay())) {
                    $nextAvailableDate = $lastEndDate->addDay();
                    break;
                }
                $lastEndDate = $reservation->end_date;
            }
        }

        // If there are no gaps, the real estate is available after the last reservation's end date
        if (is_null($nextAvailableDate)) {
            $nextAvailableDate = $lastEndDate->addDay();
        }

        return $nextAvailableDate->toDateString();
    }

    public function destroy($id)
    {
        $realEstate = RealEstate::findOrFail($id);

        // (File::exists($realEstate->main_image)) ? File::delete($realEstate->main_image) : null;
        // foreach ($realEstate->realEstateImages()->pluck('image') as $image) {
        //     (File::exists($image)) ? File::delete($image) : null;
        // }
        $realEstate->delete();

        return $this->apiResponse(null, "The real estate of id = $id was deleted successfully", 200);
    }
}
