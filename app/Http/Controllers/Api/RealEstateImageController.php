<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RealEstateImages;
use App\Http\Traits\SaveImageTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RealEstate\UpdateRealEstateImagesRequest;
use App\Http\Resources\RealEstate\RealEstateImageResource;
use Illuminate\Support\Facades\File;
use App\Http\Traits\ApiResponseTrait;

class RealEstateImageController extends Controller
{
    use SaveImageTrait, ApiResponseTrait;

    public function index($id)
    {
        $realEstateImages = RealEstateImages::where('real_estate_id', $id)->get();
        return $this->apiResponse(RealEstateImageResource::collection($realEstateImages), 'All Images for this Real Estate', 200);
    }

    public function store(UpdateRealEstateImagesRequest $request)
    {
        $final_array = [];
        foreach ($request->file('images') as $Image) {
            $image_name_DataBase = $this->saveImages($Image, 'Images/RealEstate', 400, 400);
            $new_image = RealEstateImages::create(['real_estate_id' => $request->real_estate_id, 'image' => $image_name_DataBase]);
            array_push($final_array, $new_image);
        }

        return $this->apiResponse(RealEstateImageResource::collection($final_array), 'The Images has been created successfully', 200);
    }

    public function destroy($id)
    {
        $realEstateImage = RealEstateImages::find($id);
        (File::exists($realEstateImage->image)) ? File::delete($realEstateImage->image) : Null;
        $realEstateImage->delete();
        return $this->apiResponse(null, 'The real estate image has been deleted successfully', 200);
    }
}
