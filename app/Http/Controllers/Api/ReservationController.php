<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Models\Invoice;
use App\Models\RealEstate;
use App\Models\Reservation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Resources\Reservation\ReservationResource;
use App\Http\Requests\Api\Reservation\ReservationRequest;
use App\Http\Requests\Api\Reservation\ReservationAvailabilityRequest;
use App\Http\Resources\Customer\CustomerResource;
use App\Models\Customer;

class ReservationController extends Controller
{
    use ApiResponseTrait;

    public function allReservations()
    {
        $reservations = Reservation::paginate(50);

        return $this->apiResponse(ReservationResource::collection($reservations)->resource, "All reservations", 200);
    }

    public function index($id)
    {
        $realEstate = RealEstate::findOrFail($id);

        return $this->apiResponse(ReservationResource::collection($realEstate->reservations()->paginate(config('constants.PAGINATE_NUMBER')))->resource, "All the reservations of real estate id = $id", 200);
    }

    public function customerReservations($id)
    {
        $customer = Customer::findOrFail($id);

        return $this->apiResponse(ReservationResource::collection($customer->reservations()->paginate(config('constants.PAGINATE_NUMBER')))->resource, "All the reservations of customer id = $id", 200);
    }

    public function show($id)
    {
        $reservation = Reservation::findOrFail($id);

        return $this->apiResponse(new ReservationResource($reservation), "The reservation details of id = $id", 200);
    }

    public function store(ReservationRequest $request)
    {
        $realEstate = RealEstate::findOrFail($request->real_estate_id);
        $start_date = new Carbon($request->start_date);
        $end_date = new Carbon($request->end_date);
        $availability = $this->checkAvailabilityExtend($request->real_estate_id, $start_date, $end_date);
        if ($availability->count() == 0) {
            $reservation = Reservation::create([
                'real_estate_id'        => $request->real_estate_id,
                'start_date'            => $request->start_date,
                'end_date'              => $request->end_date,
                'customer_id'           => $request->customer_id,
                'price'                 => $request->price,
                'insurance'             => $request->insurance,
            ]);
            $user = $realEstate->user;
            $user->wallet_balance = $user->wallet_balance + $request->price + $request->insurance;
            $user->save();
            $reservationPriceInvoice = Invoice::create([
                'user_id'                  =>  $user->id,
                'real_estate_id'           =>  $request->real_estate_id,
                'reservation_id'           =>  $reservation->id,
                'type'                     =>  1,
                'price'                    =>  $request->price,
                'details'                  =>  "auto created invoice from reservation $reservation->id for the price",
            ]);
            $reservationInsuranceInvoice = Invoice::create([
                'user_id'                  =>  $user->id,
                'real_estate_id'           =>  $request->real_estate_id,
                'reservation_id'           =>  $reservation->id,
                'type'                     =>  3,
                'price'                    =>  $request->insurance,
                'details'                  =>  "auto created invoice from reservation $reservation->id for the insurance",
            ]);

            return $this->apiResponse(new ReservationResource($reservation), "The reservation has been created successfully!", 201);
        }
        return $this->apiResponse(ReservationResource::collection($availability), "The reservation has not been created because there are other reservation in the period you chose!", 406);
    }

    public function update(ReservationRequest $request, $id)
    {
        $start_date = new Carbon($request->start_date);
        $end_date = new Carbon($request->end_date);
        $availability = $this->checkAvailabilityExtend($request->real_estate_id, $start_date, $end_date);
        if (($availability->count() == 1 && $availability->first()->id == $id) || $availability->count() == 0) {
            $reservation = Reservation::findOrFail($id);
            if ($request->price != $reservation->price || $request->insurance != $reservation->insurance) {
                //update user wallet
                $user = $reservation->RealEstate->user;
                $user->wallet_balance = $user->wallet_balance - $reservation->price - $reservation->insurance + $request->price + $request->insurance;
                $user->save();

                //update invoices with new prices
                foreach ($reservation->invoices as $invoice) {
                    if ($invoice->type == 1) {
                        $invoice->price = $request->price;
                        $invoice->save();
                    } elseif ($invoice->type == 3) {
                        $invoice->price = $request->insurance;
                        $invoice->save();
                    }
                }
            }
            $reservation->update([
                'real_estate_id'        => $request->real_estate_id,
                'start_date'            => $request->start_date,
                'end_date'              => $request->end_date,
                'customer_id'           => $request->customer_id,
                'price'                 => $request->price,
                'insurance'             => $request->insurance
            ]);



            return $this->apiResponse(new ReservationResource($reservation), "The reservation has been updated successfully!", 200);
        }
        return $this->apiResponse(ReservationResource::collection($availability), "The reservation has not been updated because there are other reservation in the period you chose!", 406);
    }

    public function destroy($id)
    {
        $reservation = Reservation::findOrFail($id);
        $user = $reservation->RealEstate->user;
        $user->wallet_balance = $user->wallet_balance - $reservation->price - $reservation->insurance;
        $user->save();
        $reservation->invoices()->delete();
        $reservation->delete();

        return $this->apiResponse(null, "The reservation has been deleted successfully!", 200);
    }

    public function checkAvailability($id, ReservationAvailabilityRequest $request)
    {
        $start_date = new Carbon($request->start_date);
        $end_date = new Carbon($request->end_date);
        $reservation = $this->checkAvailabilityExtend($id, $start_date, $end_date);
        return $this->apiResponse(ReservationResource::collection($reservation)->resource, "There reservations between $start_date and $end_date", 200);
    }

    public function checkAvailabilityExtend($id, $start_date, $end_date)
    {
        $reservation = Reservation::where('real_estate_id', $id)->where(function ($query) use ($start_date, $end_date) {
            $query->whereBetween('start_date', [$start_date->format('Y-m-d'), $end_date->format('Y-m-d')])
                ->orWhereBetween('end_date', [$start_date->format('Y-m-d'), $end_date->format('Y-m-d')])
                ->orWhere(function ($query) use ($start_date, $end_date) {
                    return $query->where('start_date', '<=', $start_date)->where('end_date', '>=', $end_date);
                });
        })->paginate(config('constants.PAGINATE_NUMBER'));

        return $reservation;
    }

    public function reservationBetweenTwoDates(ReservationAvailabilityRequest $request)
    {
        $start_date = new Carbon($request->start_date);
        $end_date = new Carbon($request->end_date);
        $reservation = Reservation::whereBetween('start_date', [$start_date->format('Y-m-d'), $end_date->format('Y-m-d')])->orWhereBetween('end_date', [$start_date->format('Y-m-d'), $end_date->format('Y-m-d')])->orWhere(function ($query) use ($start_date, $end_date) {
            return $query->where('start_date', '<=', $start_date)->where('end_date', '>=', $end_date);
        })->paginate(config('constants.PAGINATE_NUMBER'));

        return $this->apiResponse(ReservationResource::collection($reservation)->resource, "There reservations between $start_date and $end_date", 200);
    }
}
