<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Setting\SettingRequest;
use App\Http\Resources\Setting\SettingResource;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Traits\ApiResponseTrait;

class SettingController extends Controller
{
    use ApiResponseTrait;

    public function update(SettingRequest $request)
    {
        $setting = Setting::first();
        $setting->update([
            'wallet'            => $request->wallet,
            'reservation'       => $request->reservation,
        ]);

        return $this->apiResponse(new SettingResource($setting), "The Settings has been updated successfully!", 200);
    }

    public function index()
    {
        $settings = Setting::first();
        return $this->apiResponse(new SettingResource($settings), "All settings", 200);
    }
}
