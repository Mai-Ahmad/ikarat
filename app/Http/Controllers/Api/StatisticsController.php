<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Statistics\MonthStatisticsRequest;
use App\Http\Requests\Api\Statistics\StatisticsRequest;
use App\Http\Requests\Api\Statistics\YearReauest;
use App\Models\RealEstate;
use App\Http\Traits\ApiResponseTrait;
use App\Models\Invoice;

class StatisticsController extends Controller
{
    use ApiResponseTrait;

    public function invoicesMovementsInYear(YearReauest $request)
    {
        $invoiceMovement = Invoice::selectRaw('MONTH(created_at) as month, 
        SUM(CASE WHEN type IN (0,2) THEN price ELSE 0 END) as outcome, 
        SUM(CASE WHEN type IN (1,3) THEN price ELSE 0 END) as income')
            ->whereYear('created_at', $request->year)
            ->groupBy('month')
            ->get();
        return $this->apiResponse($invoiceMovement, "Invoice movement in the year $request->year", 200);
    }

    public function expensesMonthlyInYear(StatisticsRequest $request)
    {
        $data = DB::table('invoices')
            ->select(DB::raw('MONTH(created_at) as month'), DB::raw('SUM(price) as total'))
            ->whereYear('created_at', $request->year)
            ->where('real_estate_id', $request->real_estate_id)
            ->where('type', '=', 2)
            ->groupBy(DB::raw('MONTH(created_at)'))
            ->get();

        return $this->apiResponse($data, "the sum of expenceses in $request->year in every month of real estate id = $request->real_estate_id", 200);
    }

    public function profitsMonthlyInYear(StatisticsRequest $request)
    {
        $realEstate = RealEstate::findOrFail($request->real_estate_id);

        $profits = [];

        $results = DB::table('invoices')
            ->join('real_estates', 'invoices.real_estate_id', '=', 'real_estates.id')
            ->select(DB::raw('MONTH(invoices.created_at) as month'), DB::raw('SUM(CASE WHEN invoices.type = 1 THEN invoices.price ELSE -invoices.price END) as profit'))
            ->whereYear('invoices.created_at', $request->year)
            ->where('invoices.real_estate_id', $request->real_estate_id)
            ->whereIn('invoices.type', [1, 2])
            ->groupBy(DB::raw('MONTH(invoices.created_at)'))
            ->get();

        foreach ($results as $item) {
            $profit = $item->profit * $realEstate->profit / 100;
            $profits[] = [
                'month' => $item->month,
                'profit' => $profit,
            ];
        }


        return $this->apiResponse($profits, "the profits in $request->year in every month of real estate id = $request->real_estate_id", 200);
    }


    public function reservationMonthlyPriceInYear(StatisticsRequest $request)
    {
        $data = DB::table('invoices')
            ->select(DB::raw('MONTH(created_at) as month'), DB::raw('SUM(price) as total'))
            ->whereYear('created_at', $request->year)
            ->where('real_estate_id', $request->real_estate_id)
            ->where('type', '=', 1)
            ->groupBy(DB::raw('MONTH(created_at)'))
            ->get();

        return $this->apiResponse($data, "the sum of prices of all reservation in $request->year in every month of real estate id = $request->real_estate_id", 200);
    }

    public function reservationMonthlyPrice(MonthStatisticsRequest $request)
    {
        $data = DB::table('invoices')
            ->select(DB::raw('MONTH(created_at) as month'), DB::raw('SUM(price) as total'))
            ->whereYear('created_at', $request->year)
            ->whereMonth('created_at', $request->month)
            ->where('real_estate_id', $request->real_estate_id)
            ->where('type', '=', 1)
            ->groupBy(DB::raw('MONTH(created_at)'))
            ->get();

        return $this->apiResponse($data, "the sum of prices of all reservation in $request->month in $request->year of real estate id = $request->real_estate_id", 200);
    }

    public function redervedDaysInYear(StatisticsRequest $request)
    {
        $realEstate = RealEstate::findOrFail($request->real_estate_id);
        $year = $request->year;
        $reservations = $realEstate->reservations()->where(function ($query) use ($year) {
            $query->whereYear('start_date', $year)
                ->orWhereYear('end_date', $year)
                ->orWhere(function ($query) use ($year) {
                    $query->where('start_date', '<', "$year-01-01")
                        ->where('end_date', '>', "$year-12-31");
                });
        })->get();

        $totalReservedDays = 0;

        foreach ($reservations as $reservation) {
            $startDate = max($reservation->start_date, "$year-01-01");
            $endDate = min($reservation->end_date, "$year-12-31");
            $reservedDays = (strtotime($endDate) - strtotime($startDate)) / (60 * 60 * 24) + 1;
            $totalReservedDays += max(0, $reservedDays);
        }

        return $this->apiResponse($totalReservedDays, "Total reserved days in year $year of real estate id = $request->real_estate_id", 200);
    }


    public function redervedDaysInMonthData($real_estate_id, $year)
    {
        $realEstate = RealEstate::findOrFail($real_estate_id);
        $reservations = $realEstate->reservations()->where(function ($query) use ($year) {
            $query->whereYear('start_date', $year)
                ->orWhereYear('end_date', $year)
                ->orWhere(function ($query) use ($year) {
                    $query->where('start_date', '<', "$year-01-01")
                        ->where('end_date', '>', "$year-12-31");
                });
        })->get();


        $daysReservedByMonth = array_fill(1, 12, 0);

        foreach ($reservations as $reservation) {
            $startDate = max($reservation->start_date, "$year-01-01");
            $endDate = min($reservation->end_date, "$year-12-31");
            $startMonth = (int)date('m', strtotime($startDate));
            $endMonth = (int)date('m', strtotime($endDate));
            $startDay = (int)date('d', strtotime($startDate));
            $endDay = (int)date('d', strtotime($endDate));

            if ($startMonth == $endMonth) {
                $daysReservedByMonth[$startMonth] += $endDay - $startDay + 1;
            } else {
                $daysReservedByMonth[$startMonth] += (date('t', strtotime($startDate)) - $startDay + 1);
                $daysReservedByMonth[$endMonth] += $endDay;
                for ($month = $startMonth + 1; $month < $endMonth; $month++) {
                    $daysReservedByMonth[$month] += date('t', strtotime("$year-$month-01"));
                }
            }
        }

        return $daysReservedByMonth;
    }

    public function redervedDaysInMonth(StatisticsRequest $request)
    {
        $daysReservedByMonth = $this->redervedDaysInMonthData($request->real_estate_id, $request->year);

        return $this->apiResponse($daysReservedByMonth, "Number of days reserved in every month of year $request->year of real estate id = $request->real_estate_id", 200);
    }
}
