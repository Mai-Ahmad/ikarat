<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Phone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\user\ChangePasswordUsingEmailRequest;
use App\Http\Requests\Api\user\ChangeUserPasswordRequest;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\user\UserResource;
use App\Http\Requests\Api\user\RegisterRequest;
use App\Http\Requests\Api\user\SendVerficationCodeRequest;
use App\Http\Requests\Api\user\UpdateUserRequest;
use App\Http\Resources\user\UserResourceWithoutRealEstate;
use App\Http\Traits\ApiResponseTrait;
use App\Mail\VerficationCode;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    use ApiResponseTrait;


    public function store(RegisterRequest $request)
    {
        $user = User::create([
            'name'              => $request->name,
            'email'             => $request->email,
            'password'          => Hash::make($request->password),
            'is_admin'          => false,
            'wallet_balance'    => 0,
        ]);

        foreach ($request->phones as $key => $phone) {
            $phone = Phone::create([
                'user_id'   => $user->id,
                'number'    => $phone
            ]);
        }

        return $this->apiResponse(new UserResource($user), "The user was created successfully", 201);
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update([
            'name'      => $request->name,
            'email'     => $request->email,
        ]);
        $user->save();

        return $this->apiResponse(new UserResource($user), 'The user updated successfully', 200);
    }

    public function changePassword(ChangePasswordUsingEmailRequest $request)
    {
        $user = User::where('email', $request->email)->first();
        $user->update([
            'password'  => Hash::make($request->password),
        ]);
        $user->save();

        return $this->apiResponse(true, 'The user password has been changed successfully', 200);
    }

    public function index()
    {
        $users = User::paginate(config('constants.PAGINATE_NUMBER'));

        return $this->apiResponse(UserResource::collection($users)->resource, '20 real estate per page is returned!', 200);
    }

    public function show($id)
    {
        $user = User::findOrFail($id);

        return $this->apiResponse(new UserResource($user), "The user details of id = $id", 200);
    }

    public function auth()
    {
        $user = Auth::user();

        return $this->apiResponse(new UserResource($user), "The logged in user data", 200);
    }

    public function authUpdate(UpdateUserRequest $request)
    {
        $user = Auth::user();

        $user->update([
            'name'      => $request->name,
            'email'     => $request->email,
        ]);
        $user->save();
        return $this->apiResponse(new UserResource($user), 'The logged in user updated successfully', 200);
    }
    public function authChangePassword(ChangeUserPasswordRequest $request)
    {
        $user = Auth::user();
        $user->update([
            'password'  => Hash::make($request->password),
        ]);
        $user->save();

        return $this->apiResponse(new UserResource($user), 'The user password has been changed successfully', 200);
    }


    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return $this->apiResponse(null, "The user has been deleted successfully!", 200);
    }

    public function sendCode(SendVerficationCodeRequest $request)
    {
        $code = rand(1000, 9999);
        $user = User::where('email', $request->email)->first();
        Mail::to($user->email)->send(new verficationCode($code));
        return $this->apiResponse(['code' => $code], 'The code has been send to the email successfully', 200);
    }
}
