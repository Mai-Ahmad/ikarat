<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\WalletDetails;
use App\Http\Controllers\Controller;
use App\Http\Resources\WalletDetails\WalletDetailsResource;
use App\Http\Requests\Api\WalletDetails\WalletDetailsRequest;
use App\Http\Traits\ApiResponseTrait;

class WalletDetailsController extends Controller
{
    use ApiResponseTrait;

    public function store(WalletDetailsRequest $request)
    {
        $user = User::findOrFail($request->user_id);
        $user->wallet_balance = $user->wallet_balance + $request->price;
        $user->save();
        $walletDetails = WalletDetails::create([
            'user_id'                  =>  $request->user_id,
            'price'                    =>  $request->price,
            'details'                  =>  $request->details,
        ]);

        return $this->apiResponse(new WalletDetailsResource($walletDetails), "A new wallet details has been inserted for this user", 201);
    }

    public function update(WalletDetailsRequest $request, $id)
    {
        $walletDetails = WalletDetails::findOrFail($id);

        // return $walletDetails->user->wallet_balance;
        if ($request->user_id != $walletDetails->user_id || $request->price != $walletDetails->price) {
            $walletDetails->user->wallet_balance = $walletDetails->user->wallet_balance - $walletDetails->price;
            $walletDetails->user->save();
            $user = User::findOrFail($request->user_id);
            $user->wallet_balance = $user->wallet_balance + $request->price;
            $user->save();
        }

        $walletDetails->update([
            'user_id'                  =>  $request->user_id,
            'price'                    =>  $request->price,
            'details'                  =>  $request->details,
        ]);

        return $this->apiResponse(new WalletDetailsResource($walletDetails), "The wallet details has been updated successfully!", 201);
    }

    public function index($id)
    {
        $walletDetails = WalletDetails::where('user_id', '=', $id)->get();

        return $this->apiResponse(WalletDetailsResource::collection($walletDetails), "The wallet details of user id = $id", 201);
    }

    public function destroy($id)
    {
        $walletDetails = WalletDetails::findOrFail($id);

        $walletDetails->user->wallet_balance = $walletDetails->user->wallet_balance - $walletDetails->price;
        $walletDetails->user->save();
        $walletDetails->delete();

        return $this->apiResponse(new WalletDetailsResource($walletDetails), "The wallet details has been deleted successfully!", 201);
    }
}
