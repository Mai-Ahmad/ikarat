<?php

namespace App\Http\Controllers;

use App\Models\About;
use App\Models\Chalets;
use App\Models\Chalets_image;
use App\Models\Testimonial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;





class HomeController extends Controller
{
    function index()
    {
        $abouts=About::get();
        $items=Testimonial::get();
        $chalets = Chalets::oldest()->get();
        return view('user.home', compact('abouts','items','chalets'));
    }
    public function view_chalet($id)
    {
        $chalet = Chalets::find($id);
        $chalet_image = Chalets_image::where('chalets_id', $id)->get();
        $chalets = Chalets::oldest()->get();
        return view('user.chalets.showChalet', compact('chalet', 'chalet_image','chalets'));
    }

    public function view_about(){
        $abouts=About::get();
        $chalets = Chalets::oldest()->get();
        return view('user.about', compact('abouts','chalets'));
    }

    public function view_contact(){
        $chalets = Chalets::oldest()->get();
        return view('user.contact.contact', compact('chalets'));
    }

    

}


