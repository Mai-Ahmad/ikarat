<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Type;
use App\Models\About;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class AboutController extends Controller
{
    public function index(){
        $about = About::orderBy('created_at', 'desc')->get();
        return view('admin.about.index', ['about' => $about]); 
    
    }


    public function edit(Request $request,$id){
        $about=About::find($id);

        return view('admin.about.edit',['about'=>$about]);
    }

    public function update(Request $request,$id){
        $work=about::find($id);
       

        $work->update([
            'title'       =>$request->title,
            'sub_title' => $request->sub_title,
            'content' => $request->content,
        ]);

        return redirect(route('admin_panel.about.index'))->with(['message'=>'update successfully']);
    }

}
