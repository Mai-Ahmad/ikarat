<?php

namespace App\Http\Controllers\admin;



use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Auth;


class AdminController extends Controller
{


    public function index()
    {
        $item = Admin::orderBy('created_at', 'desc')->get();
        $admin = Auth::guard('admin')->id();


        return view('admin.admin.index', compact('item','admin' ));

    }



    public function create()
    {
        return view('admin.admin.create');
    }



    public function store(Request $request)
    {
        $item = Admin::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return redirect(route('admin_panel.admin.index'));
    }




    public function edit(Request $request, $id)
    {
        $item = Admin::find($id);
        return view('admin.admin.edit', compact('item'));
    }


    public function show($id)
    {
        $item = Admin::find($id);
        return view('admin.admin.show', compact('item'));
    }




    public function update(Request $request, $id)
    {
        $item = Admin::find($id);
        $item->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
        ]);
        return redirect(route('admin_panel.admin.index'));

    }

    public function destroy($id)
    {
        $item = Admin::find($id);
        $item->delete();
        return redirect(route('admin_panel.admin.index'));
    }

    public function changePass(Request $request, $id)
    {
        $item = Admin::find($id);
        return view('admin.admin.edit-pass', compact('item'));
    }

    public function updatepass(Request $request, $id)
    {
        $validateData = $request->validate([
            'password' => 'required',
            'confirm_password' => 'required|same:password',
        ]);

        $item = Admin::find($id);
        $item->update([
            'password'=> Hash::make($request->password),
        ]);
        return redirect(route('admin_panel.admin.index'));
    }



}
