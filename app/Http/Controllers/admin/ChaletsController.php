<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Chalets;
use App\Models\Chalets_image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ChaletsController extends Controller
{


    public function index()
    {
        $item = Chalets::orderBy('created_at', 'desc')->get();
        return view('admin.chalets.index', compact('item'));
    }



    public function create()
    {
        return view('admin.chalets.create');
    }



    public function store(Request $request)
    {
        // 
        $item = Chalets::create([
            'title' => $request->title,

        ]);

        if ($request->image != '') {
            $path = 'images/Chalets/';
            $file = $request->image;
            $filename = $file->getClientOriginalName();
            $filename =  $path . time() . $filename;
            $file->move($path, $filename);

            $item->update([
                'image' => $filename
            ]);
        }



        $images = [];
        if ($request->chalet_image != '') {
            $path = 'images/Chalets/';
            foreach ($request->chalet_image as $key => $image) {
                $imageName = $image->getClientOriginalName();
                $imageName = $path .time(). $imageName;
                $image->move(public_path('images/Chalets/'), $imageName);
                $images[] = $imageName;
            }


            foreach ($images as $key => $image) {

                Chalets_image::create([
                    'chalets_id' => $item->id,
                    'image_chalet' => $image
                ]);
            }
        }

        return redirect(route('admin_panel.Chalets.index'));
    }

    public function show($id)
    {
        $item = Chalets::find($id);
        $item_image = Chalets_image::where('chalets_id', $id)->get();
        return view('admin.chalets.item', compact('item', 'item_image'));
    }


    public function edit(Request $request, $id )
    {
        $item = Chalets::find($id);
        $item_image = Chalets_image::where('chalets_id', $id)->get();
        return view('admin.chalets.edit', compact('item', 'item_image'));
    }



    public function update(Request $request, $id)
    {
        // 

        $item = Chalets::find($id);

        if ($request->image != '') {
            (File::exists($item->image)) ? File::delete($item->image) : Null;
            $path = 'images/Chalets/';
            $file = $request->image;
            $filename = $file->getClientOriginalName();
            $filename = $path . time() . $filename;
            $file->move($path, $filename);

            $item->update([
                'image' => $filename
            ]);
        }

        $item->update([
            'title' => $request->title,
        ]);
     
           


        $images = [];
        if ($request->chalet_image != '') {

         

            $item_chalets= Chalets_image::where('chalets_id', $id)->with('category')->get();

            foreach($item_chalets as $item_chalets){
                if($item_chalets->image_chalet!='1'){
                    $item_chalets->delete();
               (File::exists($item_chalets->image_chalet)) ? File::delete($item_chalets->image_chalet): Null;

            }

        }
            $path = 'images/Chalets/';

            foreach ($request->chalet_image as $key => $image) {
                $imageName = $image->getClientOriginalName();
                $imageName = $path .time(). $imageName;
                $image->move(public_path('images/Chalets/'), $imageName);
                $images[] = $imageName;
            }
            foreach ($images as $key => $image) {

                Chalets_image::create([
                    'chalets_id' => $item->id,
                    'image_chalet' => $image
                ]);
            }
        }

        return redirect(route('admin_panel.Chalets.index'));

    }

    public function destroy($id)
    {

        $item = Chalets::find($id);
        $item_chalets= Chalets_image::where('chalets_id', $id)->with('category')->get();
        $item->delete();
      

        if($item->image!='images/avatar.png'){
            (File::exists($item->image)) ? File::delete($item->image) : Null;
        }

        foreach($item_chalets as $item_chalets){
            if($item_chalets->image_chalet!='1'){
                $item_chalets->delete();
           (File::exists($item_chalets->image_chalet)) ? File::delete($item_chalets->image_chalet): Null;

        }
    }
    return redirect(route('admin_panel.Chalets.index'));


    }
}
