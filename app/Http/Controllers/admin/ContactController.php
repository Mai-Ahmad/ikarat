<?php


namespace App\Http\Controllers\admin;

use App\Models\Contact;
use Illuminate\Http\Request;
use App\Mail\ContactMail;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class ContactController extends Controller
{

    public function index()
    {
        $contact = Contact::orderBy('created_at', 'desc')->get();
        return view('admin.contact.index', ['contact' => $contact]);
    }

    public function show($id)
    {
        $contact = Contact::find($id);
        return view('admin.contact.show', ['contact' => $contact]);
    }

   
        public function store(Request $request)
        {
          
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email',
                'subject' => 'required',
                'message' => 'required',
                
            ]);
    


            if ($validator->passes()) {

          

                $details = [
                    'name' => $request->name,
                    'email' => $request->email,
                    'subject' => $request->subject,
                    'message' => $request->message,
                ];
        
                Mail::to('maiahmad0993@gmail.com')->send(new ContactMail($details));
        
                $contact = new Contact();
                $contact->name = $request->name;
                $contact->email = $request->email;
                $contact->subject = $request->subject;
                $contact->message = $request->message;
                $contact->save();
    
               
                
            }
            else{
    
            return response()->json(['error'=>$validator->errors()]);
            }
        


      
        }


    


    public function destroy($id)
    {
        $contact = Contact::find($id);
        $contact->delete();
        return redirect()->back()->with(['message' => 'Deleted successfully  ']);
    }
}



