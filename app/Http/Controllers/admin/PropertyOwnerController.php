<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PropertyOwnerController extends Controller
{


    public function index()
    {
        $item = User::orderBy('created_at', 'desc')->get();
        return view('admin.property_owner.index', compact('item' ));
    }



    public function create()
    {
        return view('admin.property_owner.create');
    }



    public function store(Request $request)
    {
        $item = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        
        ]);

        return redirect(route('admin_panel.property_owner.index'));
    }

    public function edit(Request $request, $id)
    {
        $item = User::find($id);
        return view('admin.property_owner.edit', compact('item'));
    }


    public function show($id)
    {
        $item = User::find($id);
        return view('admin.property_owner.show', compact('item'));
    }




    public function update(Request $request, $id)
    {
        $item = User::find($id);
        $item->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
        ]);
        return redirect(route('admin_panel.property_owner.index'));

    }

    public function destroy($id)
    {
        $item = User::find($id);
        $item->delete();
        return redirect(route('admin_panel.property_owner.index'));
    }


    public function changePass(Request $request, $id)
    {
        $item = User::find($id);
        return view('admin.property_owner.edit-pass', compact('item'));
    }

    public function updatepass(Request $request, $id)
    {
        $validateData = $request->validate([
            'password' => 'required',
            'confirm_password' => 'required|same:password',
        ]);

        $item = User::find($id);
        $item->update([
            'password'=> Hash::make($request->password),
        ]);
        return redirect(route('admin_panel.property_owner.index'));
    }


  


}

