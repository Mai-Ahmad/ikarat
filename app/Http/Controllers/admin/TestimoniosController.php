<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class TestimoniosController extends Controller
{
  
    public function index(){
        $items= Testimonial::orderBy('created_at','desc')->get();
        return view('admin.testimonial.index',compact('items'));
    }

 
    public function create(){
        return view('admin.testimonial.create');
    }


 
     public function store(Request $request){

        $item=Testimonial::create([
          'name'    => $request->name,
          'description' => $request->description,
        
       
        ]);

        if($request->image != ''){
            $path = 'images/Testimonial/';

            $file = $request->image;
            $filename = $file->getClientOriginalName();
            $filename = $path.$filename;
            $file->move($path, $filename);

            $item->update([
                'image'      =>$filename
            ]);
        }
   
        
        return redirect(route('admin_panel.testimonials.index'));
    }

   
    public function show($id){
        $item=Testimonial::find($id);
        return view('admin.testimonial.item',compact('item'));
    }


    public function edit(Request $request,$id){
        $item=Testimonial::find($id);
        return view('admin.testimonial.edit',compact('item'));
    }


  
    public function update(Request $request,$id){
        $item=Testimonial::find($id);
        if($request->image != ''){
            if($item->image!='images/avatar.png'){
                (File::exists($item->image)) ? File::delete($item->image) : Null;
            }
            $path = 'images/Testimonial/';
            $file = $request->image;
            $filename = $file->getClientOriginalName();
            $filename = $path.$filename;
            $file->move($path, $filename);
            $item->update([
                'image'      =>$filename
            ]);
        }

        $item->update([
                 'name'    => $request->name,
                'description' => $request->description,
        ]);

        return redirect()->back();
    }
    
    public function destroy($id){
        $item=Testimonial::find($id);
        if($item->image!='images/avatar.png'){
            (File::exists($item->image)) ? File::delete($item->image) : Null;
        }
        $item->delete();
        return redirect()->back();
    }
}
