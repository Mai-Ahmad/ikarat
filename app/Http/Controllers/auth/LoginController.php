<?php

namespace App\Http\Controllers\auth;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Auth;

class LoginController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => ['login', 'showAdminLoginForm']]);
    }

    public function showAdminLoginForm()
    {
        return view('admin.login');
    }

    public function home()
    {
        $admin = auth()->user();
        return redirect('/admin_panel/about');
    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
            $request->session()->regenerate();
            return redirect(route('admin_panel.about.index'));
        } else {
            return back()->withErrors([
                'email' => 'The password or email is incorrect',
            ])->onlyInput('email');
        }
    }

    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
        return view('admin.login', ['url' => 'admin']);
    }

}


















