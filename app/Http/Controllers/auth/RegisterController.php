<?php

    namespace App\Http\Controllers\auth;
    use App\Models\User;
    use App\models\Admin;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\Hash;
    use Illuminate\Support\Facades\Validator;
    use Illuminate\Foundation\Auth\RegistersUsers;
    use Illuminate\Http\Request;

    class RegisterController extends Controller
    {

        public function __construct()
        {
            $this->middleware('guest');
            $this->middleware('guest:admin');
        }

        

    public function showAdminRegisterForm()
    {
        return view('register', ['url' => 'admin']);
    }



    protected function createAdmin(Request $request)
    {
        $this->validator($request->all())->validate();
        $admin = Admin::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        return redirect()->intended('login/admin');
    }



    }


    
 
