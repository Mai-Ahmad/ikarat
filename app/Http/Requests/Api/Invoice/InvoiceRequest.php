<?php

namespace App\Http\Requests\Api\Invoice;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'                  =>  'required|numeric',
            'real_estate_id'           =>  'required|numeric',
            'reservation_id'           =>  'numeric',
            'type'                     =>  'required|numeric|in:0,1,2,3',
            'price'                    =>  'required|numeric',
            'details'                  =>  'required|string',
            'image'                    =>  'image|mimes:jpg,jpeg,png,gif,webp',
        ];
    }
}
