<?php

namespace App\Http\Requests\Api\RealEstate;

use Illuminate\Foundation\Http\FormRequest;

class RealEstateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                      => 'required|string',
            'description'               => 'required|string',
            'user_id'                   => 'required|numeric',
            'address'                   => 'required|string',
            'profit'                    => 'required|numeric|min:0|max:100',
            'latitude'                  => 'required',
            'longitude'                 => 'required',
            'phone'                     => 'required',
            'instagram'                 => 'sometimes',
            'main_image'                => 'required|image|mimes:jpg,jpeg,png,gif,webp',
            'images'                    => 'required|array',
            'images.*'                  => 'required|image|mimes:jpg,jpeg,png,gif,webp',
            'color'                     => 'required|string',
            'show_reports'              => 'required|boolean',
            'show_invoices'             => 'required|boolean',
        ];
    }
}
