<?php

namespace App\Http\Requests\Api\RealEstate;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRealEstateImagesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'real_estate_id'        => 'required',
            'images'                => 'required|array',
            'images.*'              => 'required|image|mimes:jpg,jpeg,png,gif,webp',
        ];
    }
}
