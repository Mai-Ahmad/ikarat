<?php

namespace App\Http\Requests\Api\Reservation;

use Illuminate\Foundation\Http\FormRequest;

class ReservationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'real_estate_id'        => 'required|numeric',
            'customer_id'           => 'required|numeric',
            'start_date'            => 'required|date|date_format:Y-m-d',
            'end_date'              => 'required|date|date_format:Y-m-d',
            'price'                 => 'required|numeric',
            'insurance'             => 'required|numeric',
        ];
    }
}
