<?php

namespace App\Http\Requests\Api\User;

use Illuminate\Validation\Rules\Password;
use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                      => 'required|string',
            'email'                     => 'required|email|unique:users,email',
            'password'                  => [
                'required',
                Password::min(8)
                    ->letters()
                    ->mixedCase()
                    ->numbers()
                    ->symbols()
            ],
            'password_confirmation'     => 'required|same:password',
            'phones'                    => 'required|array',
            'phones.*'                  => 'required|string|unique:phones,number|distinct',
            'wallet_balance'            => 'numeric',
        ];
    }
}
