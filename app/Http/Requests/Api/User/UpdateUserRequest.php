<?php

namespace App\Http\Requests\Api\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rules\Password;


class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = ($this->route('id') != null) ? $this->route('id') : Auth::id();
        return [
            'name'                  => 'required|string',
            'email'                 => 'required|email|unique:users,email,' . $id,
            'wallet_balance'        => 'numeric',
        ];
    }
}
