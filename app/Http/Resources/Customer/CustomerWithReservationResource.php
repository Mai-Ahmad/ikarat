<?php

namespace App\Http\Resources\Customer;

use App\Http\Resources\Reservation\ReservationWithoutRelatedDataResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CustomerWithReservationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'phone'         => $this->phone,
            'email'         => $this->email,
            'birth_date'    => $this->birth_date,
            'notes'         => $this->notes,
            // 'reservations'  => $this->reservations ? ReservationWithoutRelatedDataResource::collection($this->reservations) : null,
        ];
    }
}
