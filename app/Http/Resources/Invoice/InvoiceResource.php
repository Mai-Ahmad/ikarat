<?php

namespace App\Http\Resources\Invoice;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                =>  $this->id,
            'user_id'           =>  $this->user_id,
            'real_estate_id'    =>  $this->real_estate_id,
            'reservation_id'    =>  $this->reservation_id,
            'type'              =>  $this->type,
            'price'             =>  $this->price,
            'details'           =>  $this->details,
            'image'             =>  $this->image,
        ];
    }
}
