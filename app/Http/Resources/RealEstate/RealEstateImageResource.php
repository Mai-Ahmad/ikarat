<?php

namespace App\Http\Resources\RealEstate;

use Illuminate\Http\Resources\Json\JsonResource;

class RealEstateImageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'image' => $this->image
        ];
    }
}
