<?php

namespace App\Http\Resources\RealEstate;

use App\Http\Controllers\Api\RealEstateController;
use App\Http\Controllers\Api\StatisticsController;
use App\Http\Requests\Api\Statistics\StatisticsRequest;
use App\Http\Resources\Customer\CustomerWithReservationResource;
use App\Http\Resources\user\UserResource;
use App\Http\Resources\user\UserResourceWithoutRealEstate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class RealEstateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $realEstate = new RealEstateController();
        // $statistics = new StatisticsController();
        // dd($statistics->redervedDaysInMonthData($this->id, Carbon::now()->year));

        // $customers = $this->customers()->with(['reservations' => function ($query) {
        //     $query->where('real_estate_id', $this->id);
        // }])->get()->unique();
        return [
            'id'                                => $this->id,
            'name'                              => $this->name,
            'description'                       => $this->description,
            'phone'                             => $this->phone,
            'instagram'                         => $this->instagram,
            'user'                              => $this->user ? new UserResourceWithoutRealEstate($this->user) : null,
            'address'                           => $this->address,
            'latitude'                          => $this->latitude,
            'longitude'                         => $this->longitude,
            'availablity'                       => $realEstate->getNextAvailableDate($this) == now()->toDateString(),
            'next_available_date'               => $realEstate->getNextAvailableDate($this),
            'main_image'                        => $this->main_image,
            'images'                            => $this->realEstateImages ?  RealEstateImageResource::collection($this->realEstateImages) : null,
            'profit'                            => $this->profit,
            'color'                             => $this->color,
            'show_reports'                      => $this->show_reports,
            'show_invoices'                     => $this->show_invoices,
            'customers'                         => $this->customers ? CustomerWithReservationResource::collection($this->customers->unique()) : null,
            // 'reserved_days_count_monthly'       => collect($statistics->redervedDaysInMonthData($this->id, Carbon::now()->year)),
        ];
    }
}
