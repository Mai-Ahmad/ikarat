<?php

namespace App\Http\Resources\RealEstate;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Api\RealEstateController;
use App\Http\Resources\Customer\CustomerWithReservationResource;

class RealEstateWithoutUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $realEstate = new RealEstateController();
        // $statistics = new StatisticsController();
        return [
            'id'                    => $this->id,
            'name'                  => $this->name,
            'description'           => $this->description,
            'phone'                 => $this->phone,
            'instagram'             => $this->instagram,
            'user_id'               => $this->user_id,
            'address'               => $this->address,
            'latitude'              => $this->latitude,
            'longitude'             => $this->longitude,
            'availablity'           => $realEstate->getNextAvailableDate($this) == now()->toDateString(),
            'next_available_date'   => $realEstate->getNextAvailableDate($this),
            'main_image'            => $this->main_image,
            'images'                => $this->realEstateImages ?  RealEstateImageResource::collection($this->realEstateImages) : null,
            'profit'                => $this->profit,
            'color'                 => $this->color,
            'show_reports'          => $this->show_reports,
            'show_invoices'         => $this->show_invoices,
            'customers'             => $this->customers ? CustomerWithReservationResource::collection($this->customers->unique()) : null,
            // 'reserved_days_count_monthly'       => collect($statistics->redervedDaysInMonthData($this->id, Carbon::now()->year)),
        ];
    }
}
