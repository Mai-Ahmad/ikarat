<?php

namespace App\Http\Resources\Reservation;

use App\Http\Resources\Customer\CustomerResource;
use App\Http\Resources\Invoice\InvoiceResource;
use App\Http\Resources\RealEstate\RealEstateResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ReservationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'real_estate'           => new RealEstateResource($this->realEstate),
            'customer'              => new CustomerResource($this->customer),
            'start_date'            => $this->start_date->toDateString(),
            'end_date'              => $this->end_date->toDateString(),
            'price'                 => $this->price,
            'insurance'             => $this->insurance,
            'invoices'              => $this->invoices ? InvoiceResource::collection($this->invoices) : null,
        ];
    }
}
