<?php

namespace App\Http\Resources\Reservation;

use Illuminate\Http\Resources\Json\JsonResource;

class ReservationWithoutRelatedDataResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'real_estate_id'        => $this->real_estate_id,
            'customer_id'           => $this->customer_id,
            'start_date'            => $this->start_date->toDateString(),
            'end_date'              => $this->end_date->toDateString(),
            'price'                 => $this->price,
            'insurance'             => $this->insurance,
        ];
    }
}
