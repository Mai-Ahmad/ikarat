<?php

namespace App\Http\Resources\user;

use App\Http\Resources\Phone\PhoneResource;
use App\Http\Resources\RealEstate\RealEstateWithoutUserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'name'              => $this->name,
            'is_admin'          => $this->is_admin == 1 ? true : false,
            'emial'             => $this->email,
            'phone'             => isset($this->phones) ? PhoneResource::collection($this->phones) : null,
            'real_estates'      => $this->realEstates ? RealEstateWithoutUserResource::collection($this->realEstates) : null,
            'wallet_balance'    => $this->wallet_balance
        ];
    }
}
