<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

use Illuminate\Database\Eloquent\Model;
use App\Models\Chalets_image;



class Chalets extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'image',
    ];


    public function  ChaletImage()
    {
        return $this->hasMany(Chalets_image::class);
    }
 
}


