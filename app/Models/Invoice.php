<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Invoice extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'real_estate_id',
        'reservation_id',
        'type',
        'price',
        'details',
        'image',
    ];



    // Relations

    // get the user related to this invoice
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // get the related real estate of this invoice
    public function realEstate()
    {
        return $this->belongsTo(RealEstate::class);
    }

    // get the related reservation of this invoice
    public function reservation()
    {
        return $this->belongsTo(Reservation::class);
    }




    // protected static function boot()
    // {
    //     parent::boot();

    //     static::deleting(function ($invoice) {
    //         if ($invoice->image) {
    //             Storage::delete($invoice->image);
    //         }
    //     });
    // }
}
