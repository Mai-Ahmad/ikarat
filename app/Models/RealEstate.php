<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RealEstate extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'name',
        'description',
        'address',
        'status',
        'availability_date',
        'latitude',
        'longitude',
        'main_image',
        'phone',
        'instagram',
        'profit',
        'color',
        'show_reports',
        'show_invoices',
    ];


    // Relations

    // get the user of this real estate
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // get the images of this real estate
    public function realEstateImages()
    {
        return $this->hasMany(RealEstateImages::class);
    }

    // get the reservations of this real estate
    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }

    // get the related invoices of this real estate
    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function customers()
    {
        $table_name = 'reservations';
        $table_fields = Schema::getColumnListing($table_name);
        return $this->belongsToMany(Customer::class, $table_name, 'real_estate_id', 'customer_id')->withPivot($table_fields);
    }
}
