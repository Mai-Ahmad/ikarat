<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RealEstateImages extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'real_estate_id',
        'image',
    ];


    // relations

    // get the estate of this image
    public function realEstate()
    {
        return $this->belongsTo(RealEstate::class);
    }
}
