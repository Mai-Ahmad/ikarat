<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'real_estate_id',
        'customer_id',
        'start_date',
        'end_date',
        'price',
        'insurance',
    ];

    protected $dates = ['start_date', 'end_date'];


    // relations

    // get the estate of this reservation
    public function RealEstate()
    {
        return $this->belongsTo(RealEstate::class);
    }

    // get the customer this reservation
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    // get the invoice of this reservation
    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }
}
