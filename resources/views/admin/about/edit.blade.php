@extends('admin.dashboard')

@section('content')













<div id="content" class="main-content">
    <!--  BEGIN BREADCRUMBS  -->
    <div class="secondary-nav">
        <div class="breadcrumbs-container" data-page-heading="Analytics">
            <header class="header navbar navbar-expand-sm">
                <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                    <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                        stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                        class="feather feather-menu">
                        <line x1="3" y1="12" x2="21" y2="12"></line>
                        <line x1="3" y1="6" x2="21" y2="6"></line>
                        <line x1="3" y1="18" x2="21" y2="18"></line>
                    </svg>
                </a>
                <div class="d-flex breadcrumb-content">
                    <div class="page-header">

                        <div class="page-title">
                        </div>

                        <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item "> About </li>
                                <li class="breadcrumb-item active"> Update About </li>
                            </ol>
                        </nav>

                    </div>
                </div>
            </header>
        </div>
    </div>
    <br>
    <!--  END BREADCRUMBS  -->
    <div class="row layout-spacing ">

        <!-- Content -->
        <div class="col-12" style="margin:0% 5% auto;">
            <div class="user-profile ">
                <div class="widget-content widget-content-area">
                    <div class=" " style="padding:2% 2% 0px; ">
                        <h3 class="">Update About</h3>
                    </div>

                    <div class="" style="padding: 2%;">
                        <div class="container">

                            <form class=" g-3" method="post" action="{{route('admin_panel.about.update',$about->id)}}">
                                    @method('PATCH')
                                @csrf
                                @if ($errors->any())

                                <div class="alert alert-danger">

                                    <ul style="list-style: none;margin:0">

                                        @foreach ($errors->all() as $error)

                                        <li>{{ $error }}</li>

                                        @endforeach

                                    </ul>

                                </div>

                                @endif

                                <div class="row">
                                    <div class="col-md-5">
                                        <label for="inputEmail4" class="form-label"> title </label>
                                        <input type="text" class="form-control" value="{{$about->title}}" name="title">
                                    </div>
                                    <div class="col-md-5">
                                        <label for="inputEmail4" class="form-label"> sub title </label>
                                        <input type="text" value="{{$about->sub_title}}" class="form-control"
                                            name="sub_title">
                                    </div>
                                    <div class="col-md-11">
                                        <label for="inputEmail4" class="form-label"> content </label>
                                        <textarea id="summernote" name="content" required>{{$about->content}}</textarea>
                                    </div>
                                    <div class="col-6">
                                        <div class="">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @section('script-editor')
    <script>
          $('#summernote').summernote({

      });
    </script>
    @endsection


    <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->

</div>





@endsection