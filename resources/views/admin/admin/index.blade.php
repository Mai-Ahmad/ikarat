@extends('admin.dashboard')

@section('content')
<div id="content" class="main-content">
    <!--  BEGIN BREADCRUMBS  -->
    <div class="secondary-nav">
        <div class="breadcrumbs-container" data-page-heading="Analytics">
            <header class="header navbar navbar-expand-sm">
                <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                    <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                        stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                        class="feather feather-menu">
                        <line x1="3" y1="12" x2="21" y2="12"></line>
                        <line x1="3" y1="6" x2="21" y2="6"></line>
                        <line x1="3" y1="18" x2="21" y2="18"></line>
                    </svg>
                </a>
                <div class="d-flex breadcrumb-content">
                    <div class="page-header">

                        <div class="page-title">
                        </div>

                        <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active "> Admins </li>
                            </ol>
                        </nav>

                    </div>
                </div>
            </header>
        </div>
    </div>
    <br>
    <!--  END BREADCRUMBS  -->


    <!-- Content -->
    <div class="col-12" style="margin:2% 2% auto;">
        <div class="user-profile ">
            <div class="widget-content widget-content-area">

                <div class="" style="padding: 2%;">
                    <div class="container">
                        <div class="table-responsive" id="t1">
                            <a class="btn btn-primary mb-4 mt-4 me-4" href="{{route('admin_panel.admin.create')}}"> Add
                                Admins </a>

                            <table id="myTable1" class="table table-striped table-bordered table-sm">
                                <thead>
                                    <tr>
                                        <th class="text-center" scope="col"></th>
                                        <th scope="col"> Name</th>
                                        <th scope="col">Email</th>
                                        <th scope="col"> password</th>


                                        <th class="text-center" scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>


                                    <?php $counter=1;?>
                                    @foreach($item as $item)

                                    <tr>

                                        <td>
                                            <p class="text-center">{{$counter}}</p>
                                            <span class="text-success"></span>
                                            <?php $counter++;?>
                                        </td>

                                        <td>
                                            <p class="mb-0">{{$item->name}}</p>
                                            <span class="text-success"></span>
                                        </td>
                                        <td>
                                            <p class="mb-0">{{$item->email}}</p>
                                            <span class="text-success"></span>
                                        </td>

                                        <td>

                                            <p class="mb-0">{{ \Illuminate\Support\Str::limit($item->password, 20) }}
                                            </p>
                                            <span class="text-success"></span>
                                        </td>





                                        <td class="text-center">
                                            <div class="action-btns">
                                                <a href="{{route('admin_panel.admin.show',$item->id)}}"
                                                    class="action-btn btn-view bs-tooltip me-2" data-toggle="tooltip"
                                                    data-placement="top" title="show">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                        stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                        class="feather feather-eye">
                                                        <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                        <circle cx="12" cy="12" r="3"></circle>
                                                    </svg>
                                                </a>
                                                <a href="{{route('admin_panel.admin.edit',$item->id)}}"
                                                    class="action-btn btn-edit bs-tooltip me-2" data-toggle="tooltip"
                                                    data-placement="top" title="edit">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                        stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                        class="feather feather-edit-2">
                                                        <path
                                                            d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z">
                                                        </path>
                                                    </svg>
                                                </a>

                                                @if($admin != $item->id)

                                                <a href="/admin_panel/del_admin/{{$item->id}}"
                                                    class="action-btn btn-delete bs-tooltip" data-toggle="tooltip"
                                                    data-placement="top" title="delete">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                        stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                        class="feather feather-trash-2">
                                                        <polyline points="3 6 5 6 21 6"></polyline>
                                                        <path
                                                            d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                                        </path>
                                                        <line x1="10" y1="11" x2="10" y2="17"></line>
                                                        <line x1="14" y1="11" x2="14" y2="17"></line>
                                                    </svg>
                                                </a>
                                                @endif
                                                <a href="/admin_panel/changePass/{{$item->id}}"
                                                    class="action-btn btn-edit bs-tooltip me-2" data-toggle="tooltip"
                                                    data-placement="top" title="edit password">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                        stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                        class="feather feather-lock">
                                                        <rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect>
                                                        <path d="M7 11V7a5 5 0 0 1 10 0v4"></path>
                                                    </svg> </a>
                                            </div>
                                        </td>
                                    </tr>

                                    @endforeach

                                </tbody>

                            </table>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->

    @endsection