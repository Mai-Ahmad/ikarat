@extends('admin.dashboard')

@section('content')

<div id="content" class="main-content">
    <!--  BEGIN BREADCRUMBS  -->
    <div class="secondary-nav">
        <div class="breadcrumbs-container" data-page-heading="Analytics">
            <header class="header navbar navbar-expand-sm">
                <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                    <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                        stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                        class="feather feather-menu">
                        <line x1="3" y1="12" x2="21" y2="12"></line>
                        <line x1="3" y1="6" x2="21" y2="6"></line>
                        <line x1="3" y1="18" x2="21" y2="18"></line>
                    </svg>
                </a>
                <div class="d-flex breadcrumb-content">
                    <div class="page-header">

                        <div class="page-title">
                        </div>

                        <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"> Testimonials </li>
                                <li class="breadcrumb-item active"> Testimonial </li>
                            </ol>
                        </nav>

                    </div>
                </div>
            </header>
        </div>
    </div>
    <br>
    <!--  END BREADCRUMBS  -->

    <div class="row layout-spacing ">

        <!-- Content -->
        <div class="col-12" style="margin:2% 2% auto;">
            <div class="user-profile ">
                <div class="widget-content widget-content-area">
                    <div class=" " style="padding:2% 2% 0px; ">
                        <h3 class="">Show Shalets</h3>
                    </div>
                    <div class="" style="padding: 2%; font-weight: bold;">
                        <div class="container">
                            <div class="row mb-4">
                                <div class="col-md-12 mb-4">
                                    <h5 style="display:inline"> Title : </h5>
                                    <span >{{$item->title}}</span>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <h5> Main Image </h5>
                                    <img alt="avatar" src="{{asset($item->image)}}" width="200" height="200" />
                                    <br><br>
                                </div>
                            </div>
                            <div class="row ">
                                <h5>
                                    Chalet Images
                                </h5>
                                @foreach($item_image as $item)
                                <div class="col-md-3">
                                    <img alt="avatar" src="{{asset($item->image_chalet)}}" width="200" height="200" />
                                    <br><br>
                                </div>
                                @endforeach
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>


    <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->
</div>



@endsection