@php
    $lang = App::getLocale()
@endphp
<head>
    <meta charset="utf-8">


    <title>ikarat</title>

    <link rel="shortcut icon" href="" type="image/x-icon">

    <link rel="shortcut icon" href="">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="{{asset('icons/css/docs.css')}}"/>
    <link rel="stylesheet" href="{{asset('icons/css/pygments-manni.css')}}"/>
    <link rel="stylesheet" href="{{asset('icons/icon-fonts/elusive-icons-2.0.0/css/elusive-icons.min.css')}}"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"/>
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"/>
    <link rel="stylesheet" href="{{asset('icons/icon-fonts/map-icons-2.1.0/css/map-icons.min.css')}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/octicons/4.4.0/font/octicons.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/typicons/2.0.9/typicons.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/weather-icons/2.0.10/css/weather-icons.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/2.8.0/css/flag-icon.min.css"/>
    <link rel="stylesheet" href="{{asset('icons/dist/css/bootstrap-iconpicker.css')}}"/>

{{--    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">--}}
  

        <link href="{{ asset('ltr/light/loader.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('ltr/dark/loader.css') }}" rel="stylesheet" type="text/css" />
        <script src="{{ asset('js/loader.js') }}"></script>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
        <link href="{{ asset('ltr/bootstrap.min.css') }} " rel="stylesheet" type="text/css" />

        <link href="{{ asset('ltr/light/plugins.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('ltr/dark/plugins.css') }}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->

        <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
        <link href="{{ asset('ltr/apexcharts.css') }} " rel="stylesheet" type="text/css">
        <link href="{{ asset('ltr/light/dash_1.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('ltr/dark/dash_1.css') }} " rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->
 
        <!--  BEGIN CUSTOM STYLE AJAX SEARCH  -->
{{--    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>


    
    <!--  END CUSTOM STYLE AJAX SEARCH  -->

    <style>
        form div{
            margin: 15px;
        }

        form .note-editor.note-frame {
            margin: 5px 0px;
        } 
    </style>

    <!--  Icon  -->



    <!--  End Icon  -->

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">

</head>



