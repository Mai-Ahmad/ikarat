@component('mail::message')
# Your verfication code is : {{ $code }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
