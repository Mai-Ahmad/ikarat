@extends('user.main')

@section('content')

        <!-- main-area -->
        <main>
          
            <!-- breadcrumb-area -->
            <section class="breadcrumb-area d-flex align-items-center" style="background-image:url({{asset('assets/img/slider/background_3.jpg')}}">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-2">
                            <div class="breadcrumb-wrap text-center">
                                <div class="breadcrumb-title mb-30">
                                    <h2>About Us</h2>                                    
                                </div>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">About us</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- breadcrumb-area-end -->
			      <!-- about-area -->
            <section id="about" class="about-area about-page about-p pt-120 pb-120 p-relative">
                <div class="container">
                    <div class="align-items-center">
                      <div class="about-content s-about-content pl-30">
                      @foreach($abouts as $about)

                        <div class="about-title second-atitle">
                          <span>About Us</span>
                          <h2>{{$about->title}}</h2>
                          <p>{{$about->sub_title}}</p>
                        </div>
                        <div class="about-info">
                          <p>{!!$about->content!!}</p>
                    
                        </div>
                        @endforeach
                      </div>
                    </div>
                </div>
            </section>
            <!-- about-area-end -->



        </main>
        <!-- main-area-end -->
        <!-- footer -->
        @endsection