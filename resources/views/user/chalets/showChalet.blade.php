

@extends('user.main')

@section('content')

        <!-- main-area -->
        <main>
            <!-- breadcrumb-area -->
            <section class="breadcrumb-area d-flex align-items-center" style="background-image:url({{asset($chalet->image)}})">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-2">
                            <div class="breadcrumb-wrap text-center">
                                <div class="breadcrumb-title mb-30">
                                    <h2>{{$chalet->title}}</h2>                                    
                                </div>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">{{$chalet->title}}</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- breadcrumb-area-end -->
			      <!-- gallery-area -->
            <section id="work" class="pt-120 pb-120">
              <div class="container">                  
                <div class="portfolio wow fadeInUp animated" data-animation="fadeInDown animated" data-delay=".2s">
                    <div class="row">
                    @foreach($chalet_image as $chalet_image)
                      <div class="col-md-6 col-lg-4">
                        <a class="gallery-link popup-image" href="{{asset($chalet_image->image_chalet)}}">
                          <figure class="gallery-image">
                            <img src="{{asset($chalet_image->image_chalet)}}" class="gallery_img" alt="protfolio-img01">
                          </figure>
                        </a>
                      </div>
                      @endforeach
                    </div>
                  </div>
              </div>
            </section>
            <!-- gallery-area-end -->
        </main>
        <!-- main-area-end -->
        @endsection