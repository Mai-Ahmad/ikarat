@extends('user.main')

@section('content')
<main>
  <!-- breadcrumb-area -->
  <section class="breadcrumb-area d-flex align-items-center"
    style="background-image: url({{asset('assets/img/slider/background_2.jpeg')}})">
    <div class="container">
      <div class="row">
        <div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-2">
          <div class="breadcrumb-wrap text-center">
            <div class="breadcrumb-title mb-30">
              <h2>Contact Us</h2>
            </div>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item">
                  <a href="index.html">Home</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                  Contact us
                </li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- breadcrumb-area-end -->




  <!-- contact-area -->
  <section id="contact" class="contact-area contact-bg gray-bg pt-120 pb-120 p-relative fix">
    <div class="container">
      <form id="contactForm" method="post" action="/contact" class="contact-form wow fadeInUp animated"
        data-animation="fadeInDown animated" data-delay=".2s">
        @csrf
 
        <div class="row">
          <div class="col-lg-12">
            <div class="contact-field p-relative c-name mb-40">
              <input type="text" id="name" name="name" placeholder="Write here  Jhonathan Doe" />
              <span class="text-danger error-text name_err"></span>
            </div>
          </div>
          <div class="col-lg-12">
            <div class="contact-field p-relative c-email mb-40">
              <input type="email" id="email" name="email" placeholder="Write here youremail" />
              <span class="text-danger error-text email_err"></span>
            </div>
          </div>
          <div class="col-lg-12">
            <div class="contact-field p-relative c-subject mb-40">
              <input type="text" id="subject" name="subject" placeholder="I would like to discuss" />
              <span class="text-danger error-text subject_err"></span>
            </div>
          </div>
          <div class="col-lg-12">
            <div class="contact-field p-relative c-message mb-45">
              <textarea name="message" id="message" cols="30" rows="10" placeholder="Write comments"></textarea>
              <span class="text-danger error-text message_err"></span>
            </div>
          </div>
          <div class="col-lg-12">
            <div class="send-btn">
              <button id="submit" type="submit" class="btn ss-btn">Send Message</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </section>
  <!-- contact-area-end -->
</main>
<!-- Modal -->
<div class="modal fade mt-4" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      </div>
      <div class="modal-body">
        <p>Sent successfully. Thank you for contacting us
        <p>
      </div>
      <div class="modal-footer">
        <button type="button" id="ok" class="btn btn-base  border-radius-5" data-bs-dismiss="modal">OK</button>

      </div>
    </div>
  </div>
</div>

@endsection