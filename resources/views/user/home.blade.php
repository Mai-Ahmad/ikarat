

@extends('user.main')

@section('content')


  <!-- main-area -->
  <main>
    <!-- slider-area -->
    <section id="home" class="slider-area fix p-relative">
      <div class="slider-active">
      @foreach($chalets as $chalet)
        <div class="single-slider slider-bg d-flex align-items-center"
          style="background-image:url(/{{$chalet->image}})">
          <div class="container">
            <div class="row">
              <div class="col-lg-8">
                <div class="slider-content s-slider-content text-left">
                  <h2 data-animation="fadeInUp" data-delay=".4s">{{$chalet->title}}</h2>
                  <div class="slider-btn mt-55">
                    <a href="/view_chalet/{{$chalet->id}}" class="btn ss-btn" data-animation="fadeInRight" data-delay=".8s">View Pictures</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </section>
    <!-- slider-area-end -->
    <!-- about-area -->
    <section id="about" class="about-area gray-bg about-p pt-120 pb-120 p-relative">
      <div class="container">
        @foreach($abouts as $about)
        <div class="align-items-center">
          <div class="about-content s-about-content pl-30">
            <div class="about-title second-atitle">

              <span>About Us</span>

              <h2>{{$about->title}}</h2>
              <p>{{$about->sub_title}}</p>
            </div>
            <div class="about-info">
              <p> {!!$about->content!!} </p>

            </div>

            <a href="#" class="btn ss-btn">Get Started</a>
          </div>
        </div>
      </div>
      @endforeach
    </section>
    <!-- about-area-end -->

    <!-- Instagram Photo-->
    <section id="services" class="services-area pt-113 pb-150">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-xl-8 col-lg-10">
            <div class="section-title text-center pl-40 pr-40 mb-80">
              <span>Best photo</span>
              <h2>Instagram Pictures</h2>
            </div>
          </div>
        </div>
        <div class="row">
          <!--Instagram Photo 1-->
          <div class="col-md-6 col-lg-4">
            <div class="single-services mb-30">
              <div class="services-thumb">
                <a href="#">
                  <img src="{{asset('assets/img/gallery/background_1.jpeg')}}" class="gallery_img" alt="img">
                </a>
              </div>
            </div>
          </div>
          <!--Instagram Photo 2-->
          <div class="col-md-6 col-lg-4">
            <div class="single-services mb-30">
              <div class="services-thumb">
                <a href="#">
                  <img src="{{asset('assets/img/gallery/background_1.jpg')}}" class="gallery_img" alt="img">
                </a>
              </div>
            </div>
          </div>
          <!--Instagram Photo 3-->
          <div class="col-md-6 col-lg-4">
            <div class="single-services mb-30">
              <div class="services-thumb">
                <a href="#">
                  <img src="{{asset('assets/img/gallery/background_2.jpeg')}}" class="gallery_img" alt="img">
                </a>
              </div>
            </div>
          </div>
          <!--Instagram Photo 4-->
          <div class="col-md-6 col-lg-4">
            <div class="single-services mb-30">
              <div class="services-thumb">
                <a href="#">
                  <img src="{{asset('assets/img/gallery/background_3.jpg')}}" class="gallery_img" alt="img">
                </a>
              </div>
            </div>
          </div>
          <!--Instagram Photo 5-->
          <div class="col-md-6 col-lg-4">
            <div class="single-services mb-30">
              <div class="services-thumb">
                <a href="#">
                  <img src="{{asset('assets/img/gallery/background_4.jpeg')}}" class="gallery_img" alt="img">
                </a>
              </div>
            </div>
          </div>
          <!--Instagram Photo 6-->
          <div class="col-md-6 col-lg-4">
            <div class="single-services mb-30">
              <div class="services-thumb">
                <a href="#">
                  <img src="{{asset('assets/img/gallery/backgorund_6.jpeg')}}" class="gallery_img" alt="img">
                </a>
              </div>
            </div>
          </div>

        </div>
      </div>
    </section>
    <!-- gallery-area-end -->


    <!-- testimonial-area -->
    <section id="testimonios" class="testimonial-area gray-bg testimonial-p pt-115 pb-185 text-center">
      <div class="container">
        <div class="row">
          <div class="col-lg-2">
          </div>
          <div class="col-lg-8">
            <div class="section-title center-align mb-40 wow fadeInDown animated" data-animation="fadeInDown animated"
              data-delay=".2s">
              <!-- <span>Experice With me</span> -->
              <h2>Testimonios</h2>
            </div>
            <div class="testimonial-active">
              @foreach($items as $item)
              <div class="single-testimonial">
                <i class="fas fa-quote-left"></i>
                <p>{!! $item->description !!}</p>
                <div class="testi-author text-center">
                  <img src="{{asset($item->image)}}" class="author_img" alt="img">
                  <div class="ta-info">
                    <h6>{{$item->name}}</h6>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
          </div>
          <div class="col-lg-2">
          </div>
        </div>
      </div>
    </section>
    <!-- testimonial-area-end -->
  </main>
  <!-- main-area-end -->

  @endsection



