  <!-- footer -->
  <footer class="footer-bg footer-p pt-100 pb-80 ">
    <div class="footer-top pb-30">
      <div class="container">
        <div class="row justify-content-between">
          <div class="col-xl-6 col-lg-6 col-sm-12 col-md-6">
            <div class="footer-widget mb-30">
              <div class="logo mb-35">
                <a href="#"><img src="{{asset('assets/img/logo/logo-white.png')}}" class="logo_footer" alt="logo"></a>
              </div>
              <div class="footer-text mb-20">
                <p>Pellentesque habitant morbi tristique senectus et netus et fames acturpis egestas. Vestibulum tortor
                  quam, feugiat vitae, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. mivitae est.
                  Mauris placerat eleifend leo. Quisque sit amet est et sapien.</p>
              </div>
              <div class="footer-social">
                <span>Follow Us</span>
                <a href="#"><i class="fab fa-facebook-f"></i></a>
                <a href="#"><i class="fab fa-twitter"></i></a>
                <a href="#"><i class="fab fa-instagram"></i></a>
                <a href="#"><i class="fab fa-google-plus-g"></i></a>
              </div>
            </div>
          </div>
          <div class="col-xl-2 col-lg-3 col-sm-6 col-md-3">
            <div class="footer-widget mb-30">
              <div class="f-widget-title">
                <h5>Chalets</h5>
              </div>
              <div class="footer-link">
                <ul>
                @foreach($chalets as $chalet)
                            <li><a href="/view_chalet/{{$chalet->id}}">chalet {{$chalet->id}}</a></li>
                            @endforeach
                </ul>
              </div>
            </div>
          </div>
          <div class="col-xl-2 col-lg-3 col-sm-6 col-md-3">
            <div class="footer-widget mb-30">
              <div class="f-widget-title">
                <h5>Links</h5>
              </div>
              <div class="footer-link">
                <ul>
                  <li><a href="/">Home</a></li>
                  <li><a href="/view_about">About Us</a></li>
                  <li><a href="#">Contact us</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="copyright-wrap">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="copyright-text text-center">
              <p>&copy; 2020 @ Golden Soft All design Zcube.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- footer-end -->