<header class="header-area">  
          <div class="header-top second-header d-none d-md-block">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="wellcome-text text-center text-lg-left">
                            <p>Welcome to our.</p>
                        </div>
                    </div>
                    <div class="col-lg-6 d-none d-lg-block">
                        <div class="header-cta text-right">
                            <ul>
                                <li>
                                    <i class="icon dripicons-phone"></i>
                                    <span>+8 12 3456897</span>
                                </li>
                                <li>
                                    <i class="icon dripicons-mail"></i>
                                    <span>info@example.com</span>
                                </li>
                                <li>
                                    <i class="icon dripicons-clock"></i>
                                    <span>Mon-Fri: 8am - 7pm</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
          </div>		
          <div id="header-sticky" class="menu-area">
            <div class="container">
              <div class="second-menu">
                <div class="row align-items-center">
                  <div class="col-xl-2 col-lg-2">
                    <div class="logo">
                        <a href="index.html"><img src="{{asset('assets/img/logo/Logo-2.png')}}" class="logo_img" alt="logo"></a>
                    </div>
                  </div>
                  <div class="col-xl-10 col-lg-9">
                    <div class="responsive"><i class="icon dripicons-align-right"></i></div>
                    <div class="main-menu text-right <text-xl-right></text-xl-right>">
                      <nav id="mobile-menu">
                        <ul>
                          <li>
                            <a href="/">Home</a>
                          </li>                                            
                          <li class="has-sub"><a href="#">Chalets <i class="fal fa-angle-down"></i></a>
                            <ul>		
                            @foreach($chalets as $chalet)
                            <li><a href="/view_chalet/{{$chalet->id}}">chalet {{$chalet->id}}</a></li>
                            @endforeach
                            </ul>
                          </li>
                          <li><a href="/view_about">About Us</a></li>
                          <li><a href="/view_contact">Contact</a></li>                                               
                        </ul>
                      </nav>
                    </div>
                  </div>                           
                </div>
              </div>
            </div>
          </div>
        </header>