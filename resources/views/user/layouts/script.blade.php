
  <!-- JS here -->
  <script src="{{asset('assets/js/vendor/modernizr-3.5.0.min.js')}}"></script>
  <script src="{{asset('assets/js/popper.min.js')}}"></script>
  <script src="{{asset('assets/js/vendor/jquery-1.12.4.min.js')}}"></script>
  <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('assets/js/one-page-nav-min.js')}}"></script>
  <script src="{{asset('assets/js/slick.min.js')}}"></script>
  <script src="{{asset('assets/js/ajax-form.js')}}"></script>
  <script src="{{asset('assets/js/paroller.js')}}"></script>
  <script src="{{asset('assets/js/wow.min.js')}}"></script>
  <script src="{{asset('assets/js/js_isotope.pkgd.min.js')}}"></script>
  <script src="{{asset('assets/js/imagesloaded.min.js')}}"></script>
  <script src="{{asset('assets/js/parallax.min.js')}}"></script>
  <script src="{{asset('assets/js/jquery.waypoints.min.js')}}"></script>
  <script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
  <script src="{{asset('assets/js/jquery.scrollUp.min.js')}}"></script>
  <!-- <script src="{{asset('assets/js/parallax-scroll.js')}}"></script> -->
  <script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
  <script src="{{asset('assets/js/element-in-view.js')}}"></script>
  <script src="{{asset('assets/js/main.js')}}"></script>

  <script>
    $(document).ready(function () {

$('#contactForm').on('submit',function (e) {
               e.preventDefault();
               var _token = $("input[name='_token']").val();
               var name = $('#name').val();
               var email = $('#email').val();
               var subject = $('#subject').val();
               var message = $('#message').val();

               $.ajax({
                  type: "POST",
                  url: "/contact",
                  data: {_token:_token, name:name, email:email,subject:subject, message:message},
                  success: function(data) {
                    console.log(data.error)
                        if($.isEmptyObject(data.error)){
                          console.log('responce');
                    $('#exampleModal').modal('show');
                        }else{
                          
                            printErrorMsg(data.error);
                        }
                    }
                   
               });
           });

           function printErrorMsg (msg) {
                $.each( msg, function( key, value ) {
                    console.log(key);
                    $('.'+key+'_err').text(value);
                });
            }

           $('#ok').on('click' , function(){
            document.getElementById("contactForm").reset();
            $('#exampleModal').modal('hide');})

       });

    

</script>

