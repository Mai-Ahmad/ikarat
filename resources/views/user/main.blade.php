<!doctype html>
<html class="no-js" lang="zxx">
@include('user.layouts.topheader')
<body>
@include('user.layouts.nav')

@yield('content')



@include('user.layouts.footer')

@include('user.layouts.script')



</body>
</html>
