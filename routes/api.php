<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CustomerController;
use App\Http\Controllers\Api\InvoiceController;
use App\Http\Controllers\Api\PhoneController;
use App\Http\Controllers\Api\RealEstateController;
use App\Http\Controllers\Api\RealEstateImageController;
use App\Http\Controllers\Api\ReservationController;
use App\Http\Controllers\Api\SettingController;
use App\Http\Controllers\Api\StatisticsController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\WalletDetailsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::group(['prefix' => 'auth'], function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    // Route::post('me', [AuthController::class, 'me']);
});
Route::post('change-password', [UserController::class, 'changePassword']);
Route::post('send-code', [UserController::class, 'sendCode']);
Route::group(['middleware' => 'auth:api'], function ($router) {
    // user routes
    Route::group(['prefix'  => 'user'], function () {
        Route::get('index', [UserController::class, 'index']);
        Route::get('show/{id}', [UserController::class, 'show']);
        Route::post('store', [UserController::class, 'store']);
        Route::post('update/{id}', [UserController::class, 'update']);
        Route::post('destroy/{id}', [UserController::class, 'destroy']);

        // update phone route
        Route::group(['prefix'  => 'phone'], function () {
            Route::post('/update/{id}', [PhoneController::class, 'update']); //updating phone using phone id
        });
        // authenticated user routes
        Route::group(['prefix'  => 'auth'], function () {
            Route::get('/', [UserController::class, 'auth']);
            Route::post('/update', [UserController::class, 'authUpdate']);
            Route::post('/change-password', [UserController::class, 'authChangePassword']);
        });
        // settings routes
        Route::group(['prefix' => 'settings'], function () {
            Route::get('index', [SettingController::class, 'index']);
            Route::post('update', [SettingController::class, 'update']);
        });

        Route::post('send-code', [UserController::class, 'sendCode']);
    });

    // customer routes
    Route::group(['prefix'  => 'customer'], function () {
        Route::get('index', [CustomerController::class, 'index']);
        Route::get('show/{id}', [CustomerController::class, 'show']);
        Route::post('store', [CustomerController::class, 'store']);
        Route::post('update/{id}', [CustomerController::class, 'update']);
        Route::post('destroy/{id}', [CustomerController::class, 'destroy']);
    });

    // real estate routes
    Route::group(['prefix'  => 'real-estate'], function () {
        Route::get('index', [RealEstateController::class, 'index']);
        Route::get('show/{id}', [RealEstateController::class, 'show']);
        Route::post('store', [RealEstateController::class, 'store']);
        Route::post('update/{id}', [RealEstateController::class, 'update']);
        Route::post('destroy/{id}', [RealEstateController::class, 'destroy']);
        // real estate images route
        Route::group(['prefix' => 'image'], function () {
            Route::get('index/{real_estate_id}', [RealEstateImageController::class, 'index']); // get the images of the real estate using real estate id
            Route::post('store', [RealEstateImageController::class, 'store']);
            Route::post('destroy/{id}', [RealEstateImageController::class, 'destroy']); //destroy the image using image id
        });
    });


    // reservation routes
    Route::group(['prefix' => 'reservation'], function () {
        Route::get('all-reservations', [ReservationController::class, 'allReservations']);
        Route::get('index/{real_estate_id}', [ReservationController::class, 'index']); //show all reservation for real estate using real estate id
        Route::get('customer/{customer_id}', [ReservationController::class, 'customerReservations']); //show all reservation for customer using customer id
        Route::get('show/{id}', [ReservationController::class, 'show']); //show reservation details using reservation id
        Route::post('store', [ReservationController::class, 'store']);
        Route::post('update/{id}', [ReservationController::class, 'update']);
        Route::post('destroy/{id}', [ReservationController::class, 'destroy']);
        Route::post('check-availability/{real_estate_id}', [ReservationController::class, 'checkAvailability']); // show if the real estate is available between 2 dates using real estate id
        Route::post('check-availability', [ReservationController::class, 'reservationBetweenTwoDates']); // show all reservation between 2 dates for all real estates
    });

    // invoice routes
    Route::group(['prefix' => 'invoice'], function () {
        // Route::post('store', [InvoiceController::class, 'store']);
        Route::get('index/{user_id}', [InvoiceController::class, 'index']); //show invoices using user id
        Route::post('store', [InvoiceController::class, 'store']);
        // Route::post('update/{id}', [InvoiceController::class, 'update']);
        // Route::post('destroy/{id}', [InvoiceController::class, 'destroy']);
    });

    // // wallet routes
    // Route::group(['prefix' => 'wallet-details'], function () {
    //     Route::get('index/{user_id}', [WalletDetailsController::class, 'index']); //show wallet details using user id
    //     Route::post('store', [WalletDetailsController::class, 'store']);
    //     Route::post('update/{id}', [WalletDetailsController::class, 'update']);
    //     Route::post('destroy/{id}', [WalletDetailsController::class, 'destroy']);
    // });

    // statistics routes
    Route::group(['prefix' => 'statistics'], function () {
        Route::post('invoices-movements-in-year', [StatisticsController::class, 'invoicesMovementsInYear']);
        Route::post('expenses', [StatisticsController::class, 'expensesMonthlyInYear']);
        Route::post('profits', [StatisticsController::class, 'profitsMonthlyInYear']);
        Route::post('reservation-price', [StatisticsController::class, 'reservationMonthlyPriceInYear']);
        Route::post('reserved-days-in-year', [StatisticsController::class, 'redervedDaysInYear']);
        Route::post('reserved-days-in-month', [StatisticsController::class, 'redervedDaysInMonth']);
        Route::post('reservation-price/month', [StatisticsController::class, 'reservationMonthlyPrice']);
    });
});
