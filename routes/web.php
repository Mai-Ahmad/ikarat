<?php

use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\admin\ChaletsController;
use App\Http\Controllers\admin\PropertyOwnerController;
use App\Http\Controllers\admin\TestimoniosController;
use App\Http\Controllers\auth\LoginController;
use App\Http\Controllers\auth\RegisterController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\admin\AboutController;

use App\Http\Controllers\admin\ContactController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SliderController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/login_view', [LoginController::class, 'showAdminLoginForm'])->name('admin_login');


Route::post('/login', [LoginController::class, 'login']);

Route::group(['prefix' => 'admin_panel','as' => 'admin_panel.'], function () {

    Route::group(['middleware' => 'auth:admin'], function () {

        Route::resource('about', AboutController::class);

        Route::resource('testimonials', TestimoniosController::class);
        Route::get('/del_testimonials/{id}', [TestimoniosController::class, 'destroy']);


        Route::resource('admin', AdminController::class);
        Route::get('/del_admin/{id}', [AdminController::class, 'destroy']);
        Route::get('/changePass/{id}', [AdminController::class, 'changePass']);
        Route::post('changePass/updatepass/{id}', [AdminController::class, 'updatepass']);

        
        Route::resource('property_owner', PropertyOwnerController::class);
        Route::get('/del_owner/{id}', [PropertyOwnerController::class, 'destroy']);
        Route::get('/changePass/{id}', [PropertyOwnerController::class, 'changePass']);
        Route::post('changePass/updatepass/{id}', [PropertyOwnerController::class, 'updatepass']);

       
        
     


        Route::resource('Chalets', ChaletsController::class);
        Route::get('/del_Chalets/{id}', [ChaletsController::class, 'destroy']);

        Route::resource('contact', ContactController::class);
        Route::get('/delContact/{id}', [ContactController::class, 'destroy']);

        Route::get('/logout', [LoginController::class, 'logout']);

    });
});




Route::get('/', [HomeController::class, 'index']);
Route::get('/view_chalet/{id}', [HomeController::class, 'view_chalet'])->name('views');
Route::get('/view_about', [HomeController::class, 'view_about']);

Route::get('/view_contact', [HomeController::class, 'view_contact']);

Route::post('/contact', [ContactController::class, 'store']);

Route::get('/privacy', function () {
    return view('privacy');
});
 //Clear route cache
 Route::get('/route-cache', function() {
     \Artisan::call('route:cache');
     return 'Routes cache cleared';
 });

 //Clear config cache
 Route::get('/config-cache', function() {
     \Artisan::call('config:cache');
     return 'Config cache cleared';
 }); 

 // Clear application cache
 Route::get('/clear-cache', function() {
     \Artisan::call('cache:clear');
     return 'Application cache cleared';
 });

 // Clear view cache
 Route::get('/view-clear', function() {
     \Artisan::call('view:clear');
     return 'View cache cleared';
 });

 // Clear cache using reoptimized class
 Route::get('/optimize-clear', function() {
     \Artisan::call('optimize:clear');
     return 'View cache cleared';
 });




     

   

      